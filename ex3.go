package main

import "fmt"

func main() {
	//Инициализация переменных
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4 //Такой вариант инициализации также возможен

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")

	//Краткая запись объявления переменной
	//только для новых переменных
	intVar := 10

	fmt.Printf("Value = %d Type = %T\n", intVar, intVar)

	//Задание.
	//1. Вывести типы всех переменных
	//2. Присвоить переменной intVar переменные userinit16 и userautoinit. Результат вывести.

	fmt.Printf("\nValues \n")

	var name string = "Андрій Борисенко"
	var n int = 40
	var b byte = 46
	var pi float32 = 3.14
	var f complex64 = 4+3i
	var isTest bool = true
	
	
   

	fmt.Printf("Value = %s Type = %T\n", name, name)
	fmt.Printf("Value = %d Type = %T\n", n, n)
	fmt.Printf("Value = %d Type = %T\n", b, b)
	fmt.Printf("Value = %g Type = %T\n", pi, pi)
	fmt.Printf("Value = %g Type = %T\n", f, f)
	fmt.Printf("Value = %t Type = %T\n", isTest, isTest)

	fmt.Printf("\nАppropriation \n")
	intVar = int(userinit16)
   fmt.Printf("Value = %d Type = %T\n", intVar, intVar)
	intVar = int(userautoinit)
   fmt.Printf("Value = %d Type = %T\n", intVar, intVar)


	

}



